<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IssueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'complainer' => 'required|max:255',
            'issue_date' => 'required',
            'issue_time' => 'required',
            'room_no' => 'required',
            'ext' => 'required',
            'description' => 'required',
            'file_name' => request()->method == 'POST'?'required':'',
        ];
    }

    public  function  messages()
    {
       return [
         'complainer.required' => 'Please enter complainer name',
           'complainer.max' => 'Please enter complainer name',
       ];
    }
}
