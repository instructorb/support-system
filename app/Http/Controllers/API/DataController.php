<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Issue;
use Illuminate\Http\Request;

class DataController extends Controller
{
    function getIssueList(){
        return  response()->json(Issue::with('division')->get());
    }
}
