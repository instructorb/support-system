<?php

namespace App\Http\Controllers;

use App\Http\Requests\IssueRequest;
use App\Models\Division;
use App\Models\Issue;
use App\Models\IssueLog;
use App\Models\Position;
use App\Models\ProblemCategory;
use App\Models\Section;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Exception;

class IssueController extends Controller
{
    //

    function  index(){
        $data['records'] = Issue::select('id','complainer','issue_date','issue_time','division_id')->get();
        return view('backend.issue.index',compact('data'));
    }

    function  create(){
        $data['positions'] = Position::pluck('name','id');
        $data['divisions'] = Division::pluck('name','id');
        $data['sections'] = [];
        $data['problem_categories'] = ProblemCategory::pluck('name','id');
        $data['selected_categories'] = [];

        return view('backend.issue.create',compact('data'));
    }

    function store (IssueRequest $request){
        $request->request->add(['issue_by' => auth()->user()->id]);
        if ($request->hasFile('file_name')){
            $name =uniqid() . '_' . $request->file('file_name')->getClientOriginalName();
            $request->file('file_name')->move('images',$name);
            $request->request->add(['file' => $name]);
        }
        DB::beginTransaction();
        try {
            $info = Issue::create($request->all());
            if ($info) {
                $info->problemCategories()->attach($request->input('problem_category_id'));
                $request->session()->flash('success', 'Issue posted successfully, with ID :' . $info->id);
            } else {
                $request->session()->flash('error', 'Issue post failed');
            }
            DB::commit();
        }catch (Exception $exception){
            DB::rollBack();
            $request->session()->flash('error','Issue delete failed');
        }
        return redirect()->route('backend.issue.index');
    }

    function  show($id){
        $data['record'] = Issue::find($id);
        return view('backend.issue.show',compact('data'));
    }

    function  getSection(Request  $request){
        $data['sections'] = Section::where('division_id',$request->division_id)->get();
        $a = "<option value=''>Select Section</option>";
        foreach ($data['sections'] as $section) {
            $a .= "<option value=''>$section->name</option>";

        }
        return $a;
    }

    function  edit($id){
        $data['record'] = Issue::find($id);
        if (!$data['record']){
            request()->session()->flash('error','Invalid Request');
            return redirect()->route('backend.issue.index');
        }
        $data['positions'] = Position::pluck('name','id');
        $data['divisions'] = Division::pluck('name','id');
        $data['sections'] = [];
        $data['problem_categories'] = ProblemCategory::pluck('name','id');

        $sc= $data['record']->problemCategories()->get();
        $id = [];
        foreach ($sc as $category)
        {
            $id[] = $category->id;
        }
        $data['selected_categories'] = $id;
        return view('backend.issue.edit',compact('data'));
    }

    function update (IssueRequest $request,$id){
        $issue = Issue::find($id);
        if ($issue){
            if ($request->hasFile('file_name')){
                $name =uniqid() . '_' . $request->file('file_name')->getClientOriginalName();
                $request->file('file_name')->move('images',$name);
                $request->request->add(['file' => $name]);
            }
            $info  = $issue->update($request->all());

            if ($info){
                $issue->problemCategories()->sync($request->input('problem_category_id'));
                $request->session()->flash('success','Issue updated successfully');
            } else {
                $request->session()->flash('error','Issue update failed');
            }
            return redirect()->route('backend.issue.index');
        }

    }

    function  destroy(Request $request,$id){
        $data['record'] = Issue::findOrFail($id);
        if (!$data['record']){
            request()->session()->flash('error','Invalid Request');
            return redirect()->route('backend.issue.index');
        }
        DB::beginTransaction();
        try{
//            $data['record']->problemCategories()->detach();
            $data['record']->delete();
            DB::commit();
            $request->session()->flash('success','Issue deleted successfully');
        }catch (Exception $exception){
            DB::rollBack();
            $request->session()->flash('error','Issue delete failed');
        }
        return redirect()->route('backend.issue.index');
    }

    function  changeStatus($id){
        $data['statuses'] = Status::pluck('name','id');
        $data['record'] = Issue::find($id);
        return view('backend.issue.change_status',compact('data'));
    }

    function  logStatus(Request $request,$id){
       $log =  IssueLog::create([
           'logged_by' => auth()->user()->id,
           'issue_id' => $id,
           'status_id' => $request->input('status_id'),
           'ip_address' => $request->ip()
        ]);



        if ($log) {
            $request->session()->flash('success', 'Status changed');
        } else {
            $request->session()->flash('error', 'Status change failed');
        }
        return redirect()->route('backend.issue.index');

    }


}
