<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Issue extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'issues';
    protected  $fillable = ['issue_date','issue_time','complainer','position_id','division_id','section_id','room_no','ext','description','file','remarks','issue_by'];

    function  division(){
        return $this->belongsTo(Division::class);
    }

    function  section(){
        return $this->belongsTo(Section::class);
    }

    function problemCategories(){
        return $this->belongsToMany(ProblemCategory::class);
    }

    function  issueLogs(){
        return $this->hasMany(IssueLog::class);
    }
}
