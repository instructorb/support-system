<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IssueLog extends Model
{
    use HasFactory;
    protected $table = 'issue_logs';

    protected $fillable = ['issue_id','logged_by','ip_address','status_id'];

    function loggedBy(){
        return $this->belongsTo(User::class,'logged_by','id');
    }

    function status(){
        return $this->belongsTo(Status::class);
    }
}
