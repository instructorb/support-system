<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;
    protected $table = 'divisions';

    protected $fillable = ['name'];

    function issues(){
        return $this->hasMany(Issue::class);
    }
}
