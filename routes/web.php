<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\IssueController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/display-message/{name?}', [\App\Http\Controllers\FirstController::class, 'displayMessage']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/division/get_section', [IssueController::class, 'getSection'])->name('division.get_section');

Route::prefix('backend/issue')->name('backend.issue.')->middleware(['auth'])->group(function(){
    Route::get('/', [IssueController::class, 'index'])->name('index');
    Route::get('/create', [IssueController::class, 'create'])->name('create');
    Route::post('/', [IssueController::class, 'store'])->name('store');
    Route::get('/{id}', [IssueController::class, 'show'])->name('show');
    Route::get('/{id}/edit', [IssueController::class, 'edit'])->name('edit')->middleware('check_support');
    Route::put('/{id}', [IssueController::class, 'update'])->name('update');
    Route::delete('/{id}', [IssueController::class, 'destroy'])->name('destroy')->middleware('check_support');;
    Route::get('change_status/{id}', [IssueController::class, 'changeStatus'])->name('change_status');
    Route::post('log_status/{id}', [IssueController::class, 'logStatus'])->name('log_status');

});

