<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssueProblemCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_problem_category', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('issue_id')->nullable();
            $table->unsignedBigInteger('problem_category_id')->nullable();
            $table->timestamps();
            $table->foreign('issue_id')->references('id')->on('issues');
            $table->foreign('problem_category_id')->references('id')->on('problem_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_problem_category');
    }
}
