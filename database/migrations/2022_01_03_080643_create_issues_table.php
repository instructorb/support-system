<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->id();
            $table->date('issue_date');
            $table->time('issue_time');
            $table->string('complainer');
            $table->unsignedBigInteger('position_id')->nullable();
            $table->unsignedBigInteger('division_id')->nullable();
            $table->unsignedBigInteger('section_id')->nullable();
            $table->string('room_no');
            $table->integer('ext');
            $table->text('description');
            $table->string('file');
            $table->string('remarks');
            $table->text('issue_by');
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->foreign('position_id')->references('id')->on('positions');
            $table->foreign('section_id')->references('id')->on('sections');         
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
