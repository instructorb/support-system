<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

           $table->string('username')->unique();
           $table->unsignedBigInteger('position_id')->nullable();
            $table->unsignedBigInteger('user_type_id')->nullable();
            $table->string('room_no')->nullable();
            $table->string('ext')->nullable();
            $table->foreign('position_id')->references('id')->on('positions');
            $table->foreign('user_type_id')->references('id')->on('user_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->dropForeign('users_position_id_foreign');
           $table->dropForeign('users_user_type_id_foreign');
           $table->dropColumn(['username','position_id','user_type_id','room_no','ext']);
        });
    }
}
