<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssueLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('issue_id')->nullable();
            
            $table->unsignedBigInteger('status_id')->nullable();
            $table->string('logged_by');
            $table->ipAddress('ip_address');
           
            $table->timestamps();
            $table->foreign('issue_id')->references('id')->on('issues');
            $table->foreign('status_id')->references('id')->on('statuses'); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_logs');
    }
}
