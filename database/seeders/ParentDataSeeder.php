<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParentDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $divisions = [
            [
                'id' => 1,
                'name' => 'अनुसन्धान महाशाखा नं. १',
                'sections' => []
            ],
            [
                'id' => 2,
                'name' => 'नीति, योजना तथा अन्तर्राष्ट्रिय सम्बन्ध महाशाखा',
                'sections' => [
                    [
                        'name' => 'IT',
                    ],
                    [
                        'name' => 'Library',
                    ],
                    [
                        'name' => 'Planning',
                    ]
                ]
            ],

        ];
        foreach ($divisions as $division){
            DB::table('divisions')->insert([
                'name'=> $division['name'],
                'id' => $division['id']
            ]);

            foreach ($division['sections'] as $section){
                DB::table('sections')->insert([
                    'name'=> $section['name'],
                    'division_id' => $division['id']
                ]);
            }
        }

        $user_classes = [
             [
                 'id' => 1,
                 'name' => 'First Class',
                 'key' => 'First_Class',

             ],
             [
                 'id' => 2,
                 'name' => 'Second Class',
                 'key' => 'Second Class',

             ],
             [
                 'id' => 3,
                 'name' => 'Third Class',
                 'key' => 'Third Class',

             ],
         ];
         foreach ($user_classes as $user_class){
             DB::table('user_classes')->insert([
                 'name'=> $user_class['name'],
                 'id' => $user_class['id'],
                 'key'=> $user_class['key'],

             ]);
         }

         $positions = [
             [
                 'id' => 1,
                 'name' => 'Section Officer',
                 'user_class_id' => 3
             ],
             [
                 'id' => 2,
                 'name' => 'Computer Operator',
                 'user_class_id' => 1
             ],
             [
                 'id' => 3,
                 'name' => 'Engineer',
                 'user_class_id' => 3
             ],
         ];
         foreach ($positions as $position){
             DB::table('positions')->insert([
                 'name'=> $position['name'],
                 'user_class_id' => $position['user_class_id'],
                 'id' => $position['id']
             ]);
         }

         $statuses = [
             [
                 'id' => 1,
                 'name' => 'New Issue',
             ],
             [
                 'id' => 2,
                 'name' => 'On Going',
             ],
             [
                 'id' => 3,
                 'name' => 'Resolve',
             ],
             [
                 'id' => 4,
                 'name' =>"Can't Resolve",
             ],
         ];
         foreach ($statuses as $status){
             DB::table('statuses')->insert([
                 'name'=> $status['name'],
                 'id' => $status['id']
             ]);
         }

        $problem_categories = [
            'Hardware Issue',
            'Software Issue',
            'Network Issue',
            'Automation Issue',
        ];
        foreach ($problem_categories as $problem_category){
            DB::table('problem_categories')->insert([
                'name'=> $problem_category,
            ]);
        }

    }
}
