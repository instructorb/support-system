@extends('layouts.backend')
@section('title','List Issue')

@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Issue Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Issue Management</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List Issue</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
               @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif

                <table class="table table-border">
                    <tr>
                        <th>SN</th>
                        <th>Complainer</th>
                        <th>Issue Date</th>
                        <th>Issue Time</th>
                        <th>Division</th>
                        <th>Action</th>
                    </tr>
                    @foreach($data['records'] as $index => $record)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>{{$record->complainer}}</td>
                            <td>{{$record->issue_date}}</td>
                            <td>{{$record->issue_time}}</td>
                            <td>{{$record->division->name}}</td>
                            <td>
                                <a href="{{route('backend.issue.change_status',$record->id)}}" class="btn btn-success">Change Status</a>

                                <a href="{{route('backend.issue.show',$record->id)}}" class="btn btn-info">Details</a>
                                @if(auth()->user()->userType->key == 'admin')

                                <a href="{{route('backend.issue.edit',$record->id)}}" class="btn btn-warning">Edit</a>

                                    @endif

                                @if(auth()->user()->userType->key == 'admin')
                                    {!! Form::open(['route' => ['backend.issue.destroy', $record->id],'method' =>'DELETE']) !!}
                                    {!! Form::submit('Delete',['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                                </form>
                                @endif
                            </td>


                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

@endsection
