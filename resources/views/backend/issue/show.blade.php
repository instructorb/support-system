@extends('layouts.backend')
@section('title','Details Issue')

@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Issue Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Issue Management</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List Issue</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
               @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif

                <table class="table table-border">
                    <tr>
                        <th>Category</th>
                        <td>
                        @foreach($data['record']->problemCategories as $category)
                                <a href="" class="btn btn-info">{{$category->name}}</a>


                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Complainer</th>
                        <td>{{$data['record']->complainer}}</td>
                    </tr>
                    <tr>
                        <th>Issue Date</th>
                        <td>{{$data['record']->issue_date}}</td>
                    </tr>
                    <tr>
                        <th>Issue Time</th>
                        <td>{{$data['record']->issue_time}}</td>
                    </tr>
                    <tr>
                        <th>Division</th>
                        <td>{{$data['record']->division->name}}</td>
                    </tr>
                    @if($data['record']->section_id)
                    <tr>
                        <th>Section</th>
                        <td>{{$data['record']->section->name}}</td>
                    </tr>
                        @endif
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Issue Logs

                <table class="table table-border">
                    <tr>
                        <th>Logged By</th>
                        <th>Status</th>
                        <th>IP</th>
                        <th>Created Date</th>
                    </tr>
                @foreach($data['record']->issueLogs as $log )
                        <tr>
                            <td>{{$log->loggedBy->name}}</td>
                            <td>{{$log->status->name}}</td>
                            <td>{{$log->ip_address}}</td>
                            <td>{{$log->created_at}}</td>
                        </tr>
                @endforeach

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

@endsection
