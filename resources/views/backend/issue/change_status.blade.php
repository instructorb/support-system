@extends('layouts.backend')
@section('title','Change Issue Status')

@section('main-content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Issue Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Issue Management</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"> Change Issue Status</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <th>Issue Description</th>
                        <td>{{$data['record']->description}}</td>
                    </tr>
                </table>
                {!! Form::open(['route' => ['backend.issue.log_status', $data['record']->id],'method' =>'POST']) !!}
                    <div class="form-group">
                        {!! Form::label('status_id','Status') !!}
                        {!! Form::select('status_id',$data['statuses'],null,['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">

                        {!! Form::submit('Change Status',['class' => 'btn btn-info']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->

@endsection
@section('js')
    <script>
        $('#section_data').hide();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function(){
            var path = "{{route('division.get_section')}}";
            $('#division_id').change(function(){
                $('#section_data').show();
                var division = $('#division_id').val();
                $.ajax({
                   url:path,
                    method:'post',
                    data:{'division_id':division},
                    success:function(response){
                       $('#section_id').html(response);
                    }
                });
            });
        });
    </script>
@endsection
