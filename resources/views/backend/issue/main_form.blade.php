<div class="form-group">
    {!!  Form::label('problem_category_id', 'Problem Category'); !!}
    {!! Form::select('problem_category_id[]', $data['problem_categories'],$data['selected_categories'],['placeholder' => 'Select Category','class' => 'form-control','multiple' => true]) !!}
</div>
<div class="form-group">
    {!!  Form::label('complainer', 'Complainer'); !!}
    {!! Form::text('complainer',null,['class' => 'form-control', 'accesskey'=>"s"]); !!}
    @include('layouts.error_message',['field' => 'complainer'])
</div>
<div class="form-group">
    {!!  Form::label('issue_date', 'Issue Date'); !!}
    {!! Form::date('issue_date',\Illuminate\Support\Carbon::now(),['accesskey'=>"d",'class' => 'form-control']); !!}
    @include('layouts.error_message',['field' => 'issue_date'])
</div>
<div class="form-group">
    {!!  Form::label('issue_time', 'Issue Time'); !!}
    {!! Form::time('issue_time',\Illuminate\Support\Carbon::now(),['class' => 'form-control']); !!}
    @include('layouts.error_message',['field' => 'issue_time'])

</div>
<div class="form-group">
    {!!  Form::label('position_id', 'Position'); !!}
    {!! Form::select('position_id', $data['positions'],null,['placeholder' => 'Select Position','class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!!  Form::label('division_id', 'Division'); !!}
    {!! Form::select('division_id', $data['divisions'],null,['placeholder' => 'Select Division','class' => 'form-control']) !!}
</div>
<div class="form-group" id="section_data">
    {!!  Form::label('section_id', 'Section'); !!}
    {!! Form::select('section_id', $data['sections'],null,['placeholder' => 'Select Section','class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!!  Form::label('room_no', 'Room'); !!}
    {!! Form::text('room_no',null,['class' => 'form-control']); !!}
    @include('layouts.error_message',['field' => 'room_no'])

</div>
<div class="form-group">
    {!!  Form::label('ext', 'Extention'); !!}
    {!! Form::number('ext',null,['class' => 'form-control']); !!}
    @include('layouts.error_message',['field' => 'ext'])

</div>
<div class="form-group">
    {!!  Form::label('description', 'Description'); !!}
    {!! Form::textarea('description',null,['class' => 'form-control','rows' => 3]); !!}
    @include('layouts.error_message',['field' => 'description'])

</div>
<div class="form-group">
    {!!  Form::label('remarks', 'Remarks'); !!}
    {!! Form::textarea('remarks',null,['class' => 'form-control','rows' => 3]); !!}
</div>
<div class="form-group">
    {!!  Form::label('File', 'File'); !!}
    {!! Form::file('file_name',['class' => 'form-control']); !!}
    @include('layouts.error_message',['field' => 'file_name'])
</div>
<div class="form-group">
    {!! Form::submit( $button . ' Issue',['class' => 'btn btn-info']); !!}
    {!! Form::reset('Clear',['class' => 'btn btn-danger']); !!}
</div>
